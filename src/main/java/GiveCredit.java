import java.util.Scanner;

public class GiveCredit {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Введите возраст");
        int age = input.nextInt();

        System.out.println("Введите имя");
        String name = input.next();

        System.out.println("Введите сумму");
        int amount = input.nextInt();

        String message = age >= 18 && !name.equals("Bob")  && amount <= age * 100
                ? "Кредит выдан"
                : "Кредит не выдан";
        System.out.println(message);
    }
}
